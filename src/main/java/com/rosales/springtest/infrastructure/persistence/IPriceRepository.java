package com.rosales.springtest.infrastructure.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.Optional;

public interface IPriceRepository extends JpaRepository<PriceEntity, Long> {

    @Query(value =
            "SELECT * FROM PRICES p " +
                    "WHERE p.id_product = :idProduct " +
                    "AND p.id_brand = :idBrand " +
                    "AND p.start_date <= :applicationDate " +
                    "AND p.end_date >= :applicationDate " +
                    "ORDER BY p.priority DESC " +
                    "LIMIT 1", nativeQuery = true
    )
    Optional<PriceEntity> findFirstByProductAndBrandAndApplicationDate(Long idProduct,
                                                                       Integer idBrand,
                                                                       LocalDateTime applicationDate);

}
